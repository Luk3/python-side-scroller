__author__ = 'Luke'

'''
    Bullet Class
    Manages bullet direction and image
'''

import pygame

class Bullet:

    # Constructor
    # Requires a location of origin and target
    def __init__(self, startLocation, targetLocation):
        self.destinationX = targetLocation[0]
        self.destinationY = targetLocation[1]
        self.image = pygame.image.load("images/bullet.png")

        self.rect = self.image.get_rect()
        self.rect.top = startLocation.y+40
        self.rect.left = startLocation.x
        self.status = "Fired"
        self.time = pygame.time.get_ticks()

    # Remove the bullet
    # Should result in explosive gif... COME BACK TO
    def explode(self):
        self.status = "Done"

    # Check if bullet out of screen
    def checkBoundaries(self):
        if self.rect.top<=0 or self.rect.top>=399:
            self.explode()
        if self.rect.left<=0 or self.rect.left>=580:
            self.explode()

    # Update the bullet's position and make checks on boundaries
    def update(self):
        if self.destinationX > self.rect.left:
            self.rect.left+=5
        elif self.destinationX < self.rect.left:
            self.rect.left-=5

        if self.destinationY > self.rect.top:
            self.rect.top+=5
        elif self.destinationY < self.rect.top:
            self.rect.top-=5

        self.checkBoundaries()

        if pygame.time.get_ticks() - self.time >=1000:
            self.explode()

    # Returns image
    def getImage(self):
        return self.image

    # Returns rect
    def getRect(self):
        return self.rect

    # Returns if bullet is finished
    def isDone(self):
        return self.status=="Done"

    # Set's bullet to be finished
    def setDone(self):
        self.status="Done"