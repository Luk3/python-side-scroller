__author__ = 'Luke'

'''
    Monster Class
    Manages Monsters health
'''

from MonsterGui import MonsterGui

class Monster:
    MAX_HEALTH = 20

    # Constructor
    # Sets health and gui
    def __init__(self):
        self.health = Monster.MAX_HEALTH
        self.gui = MonsterGui()

    # Detracts health by received value
    def loseHealth(self, value):
        self.health-=value

    # Returns true if alive
    def isAlive(self):
        if self.health>0:
            return True
        return False

    # Reset's health
    def reset(self):
        self.health = Monster.MAX_HEALTH

    # Returns gui
    def getGui(self):
        return self.gui