__author__ = 'Luke'

'''
    Monster Gui
    Manages the monster's position and movements
'''

import pygame
import glob
import random

class MonsterGui:
    # Constructor
    # Sets the images and positioning
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.imagePosition = 0
        self.images = glob.glob("images/monster*.png")
        self.image = pygame.image.load(self.images[self.imagePosition])

        self.rect = self.image.get_rect()
        self.rect.top = random.uniform(315, 350)
        self.rect.left = 600

        self.moving=True
        self.direction="right"
        self.speed = random.uniform(1, 8)

    # Update image
    def updateImage(self):
        if self.imagePosition>=5:
            self.imagePosition=0
        else:
            self.imagePosition+=1

        self.image = pygame.image.load(self.images[self.imagePosition])

    # Update position
    def move(self):
        self.rect.left-=self.speed

        self.updateImage()
        self.moving=True

    # End moving
    def endMove(self):
        self.moving=False

    # Called externally to update
    def update(self):
        if self.moving:
            self.move()

    # Returns image
    def getImage(self):
        return self.image

    # Returns rect
    def getRect(self):
        return self.rect