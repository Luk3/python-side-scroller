__author__ = 'Luke'

'''
    Player Class
        Moves, Shoots, Takes Damage
'''

import PlayerGui

class Player:
    MAX_HEALTH = 20

    # Constructor
    # Sets health and gui
    def __init__(self, playerGui):
        self.health = Player.MAX_HEALTH
        self.gui = playerGui

    # Detracts health by received value
    def loseHealth(self, value):
        self.health-=value

    # Returns true if alive
    def isAlive(self):
        if self.health>0:
            return True
        return False

    # Reset the player
    def reset(self):
        self.health = Player.MAX_HEALTH
