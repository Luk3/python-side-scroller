__author__ = 'Luke'

'''
    PlayerGui Class
        Moves, Fires Bullets
    Displays Player and keeps track of location, movement, actions

    TODO:
        Up/Down movement
            Add images and boundaries
            Add key events
'''

import pygame

class PlayerGui:

    # Max jump height
    maxJumpHeight = 150

    # Constructor
    # Sets the images and positioning
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.imagePosition = 1
        self.image = pygame.image.load("images/playerRight2.png")

        self.rect = self.image.get_rect()
        self.rect.top = 300
        self.rect.left = 120

        self.moving=False
        self.jumpState="None"
        self.direction="left"

    # Reset player
    def reset(self):
        self.imagePosition=1
        self.rect.top = 300
        self.rect.left = 120

        self.moving=False
        self.jumpState="None"
        self.direction="left"

    # Update the image
    def updateImage(self):
        if self.imagePosition==3:
            self.imagePosition=1
        else:
            self.imagePosition+=1

        self.image = pygame.image.load("images/player"+self.direction+str(self.imagePosition)+".png")

    # Update the position, takes direction (str)
    def move(self, direction):
        if direction=="left" and self.rect.left>0:
            self.rect.left-=2
        elif self.rect.right<600:
            if self.moving:
                self.rect.right+=2

        self.direction=direction
        self.updateImage()

    # Primarily to set moving=True.  Takes direction (str)
    def doMove(self, direction):
        self.direction = direction
        self.moving = True

    # Set moving=False
    def endMove(self):
        self.moving=False

    # End Jumping
    def endJump(self):
        self.jumpState="None"

    # Do jump...  Does the bookkeeping for jumping
    def doJump(self):
        if self.jumpState=="Up":
            self.rect.top-=7
            if self.rect.top<=PlayerGui.maxJumpHeight:
                self.jumpState="Down"
        else:
            if self.rect.top<300:
                self.rect.top+=2
                if self.rect.top==300:
                    self.jumpState="Up"

    # Update the position and jumping
    def update(self):
        if self.moving:
            self.move(self.direction)
        else:
            self.move("right")
        if not self.jumpState=="None":
            self.doJump()
        elif self.rect.top<300:
            self.rect.top+=2

    # Called to initiate jumping
    def jump(self):
        if self.jumpState=="None" and self.rect.top>=300:
            self.rect.top+=5
            self.jumpState="Up"

    # Get image
    def getImage(self):
        return self.image

    # Get rect
    def getRect(self):
        return self.rect