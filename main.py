__author__ = 'Luke'

'''
    Main file to run program.
    Organizes all the necessary components and runs them.
'''

import pygame,sys, random
from Player import Player
from PlayerGui import PlayerGui
from Monster import Monster
from MonsterGui import MonsterGui
from Bullet import Bullet
import inputbox

# Spawn Management
MaxMonsters = 5
spawnTimer = 2500

pygame.init()

# Set up display
screen = pygame.display.set_mode((600, 400))
run = True
inPlay = False

    # Back-Background
backBackground = pygame.image.load("images/backBackground.png")
backBackgroundRect = backBackground.get_rect()
backBackgroundSurface = pygame.Surface((600, 400))

    # Background
background = pygame.image.load("images/background.png")    # Load Background image
backgroundRect = background.get_rect()
maxXBackground = backgroundRect.right - 600
backgroundSurface = pygame.Surface((600, 400))
background.set_colorkey((0,0,0))

    # Foreground
foreground = pygame.image.load("images/foreground.png")
foregroundRect = foreground.get_rect()
maxXForeground = foregroundRect.right - 600
foregroundSurface = pygame.Surface((600, 250))

    # Start Image
startImage = pygame.image.load("images/startImage.png")
startImageRect = startImage.get_rect()
startImageRect.top = 50
startImageRect.left = 100

    # Score Image
nameImage = pygame.image.load("images/enterName.png")
nameImageRect = nameImage.get_rect()
nameImageRect.top = 75
nameImageRect.left = 200

    # Background Positions
backBackgroundPosition=0
backgroundPosition = 0
foregroundPosition=0

# Font
font = pygame.font.SysFont("calibri",25)
fontI = pygame.font.SysFont("calibri", 12)

# Set up Player
playerGui = PlayerGui()
player = Player(playerGui)
bullets = []
score=0

# Set Up Monsters
monsters = []

# Resets Game
# Resets the backgrounds, Player.
# Clears bullets and monsters and score
def reset():
    # Reset background positions
    global backBackgroundPosition
    backBackgroundPosition=0
    global backgroundPosition
    backgroundPosition= 0
    global foregroundPosition
    foregroundPosition=0

    # Reset player
    player.reset()
    playerGui.reset()
    global score
    score=0

    # Reset others
    bullets.clear()
    monsters.clear()

# Start the game
def startGame():
    reset()
    global inPlay
    inPlay=True

# Handles the score
def handleScores():
    # Inner class to hold player names and scores
    class PlayerScore:
        def __init__(self, name, score):
            self.playerName=name
            self.playerScore=score

        def getScore(self):
            return self.playerScore

        def getName(self):
            return self.playerName

        def setName(self, name):
            self.playerName=name

    scoreList=[]
    doAsk=False

    # Get all scores and names
    fin = open("scores.txt", 'r')
    for line in fin:
        scoreList.append(PlayerScore(line.split()[0], int(line.split()[1])))
    fin.close()

    # Check if user score is in top ten
    for value in scoreList:
        if score > value.getScore():
            doAsk=True
            playerIndex=value
            scoreList.append(PlayerScore("YOURNAME", score))
            break

    # If less than ten scores is freebie
    if len(scoreList)<10 and doAsk==False and score>0:
        doAsk=True
        scoreList.append(PlayerScore("YOURNAME", score))

    # Sort Scores
    scoreList = sorted(scoreList, key=lambda PlayerScore: PlayerScore.getScore())

    # Keep at ten
    while len(scoreList)>10:
        scoreList.pop(0)

    # y is position value for scores on y axis
    y = 250
    # Print the scores
    for line in reversed(scoreList):
        printScore = fontI.render('{:<20}  {:>15}'.format(line.getName(), str(line.getScore())), True, (255, 255, 255))
        screen.blit(printScore, (10, y))
        y+=12

    # Get player name
    if doAsk:
        # Ask user for Name
        screen.blit(nameImage, nameImageRect)
        playerName = inputbox.ask(screen, 'Your Name')
        for i in range(len(scoreList)):
            if scoreList[i].getName()=="YOURNAME":
                scoreList[i].setName(playerName)
                doAsk=False
        if doAsk:
            scoreList.append(PlayerScore(playerName, score))

    fin = open("scores.txt", 'w')

    # Write Back
    fin.truncate()
    for line in reversed(scoreList):
        print(line.getName()+" "+str(line.getScore()), file=fin)

    fin.close()

# Ends the game
# Score is settled and reset is called
def endGame():
    global inPlay, playerName
    # if score greater than lowest high score
    handleScores()
    inPlay=False
    reset()

# Handle user interaction
def handleInput():
    for event in pygame.event.get():
        # Check if quit
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

        # If game is in session
        if (inPlay):
            # Check if player interaction
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_a:
                    # go left
                    playerGui.doMove("left")
                if event.key == pygame.K_d:
                    # go right
                    playerGui.doMove("right")
                if event.key == pygame.K_SPACE:
                    # Shoot
                    bullets.append(Bullet(playerGui.getRect(), pygame.mouse.get_pos()))
                if event.key == pygame.K_w:
                    # jump
                    playerGui.jump()

            # Check jump
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_a or event.key == pygame.K_d:
                    playerGui.endMove()
                if event.key == pygame.K_w:
                    playerGui.endJump()

        # Else check if ready to start game
        else:
            if event.type==pygame.KEYDOWN:
                if event.key==pygame.K_SPACE:
                    startGame()

# Moves the backgrounds
def moveBackgrounds():
    global backBackgroundPosition
    global foregroundPosition
    global backgroundPosition

    # Advance the background
    backBackgroundSurface.blit(backBackground, (0,0), (backBackgroundPosition,0,600+backBackgroundPosition, 400))
    backBackgroundSurface.blit(background, (0,0), (backgroundPosition,0,600+backgroundPosition, 400))

    # Advance the foreground
    backBackgroundSurface.blit(foreground, (0,200), (foregroundPosition, 0, 600+foregroundPosition, 400))
    screen.blit(backBackgroundSurface, (0,0))

    backBackgroundPosition+=.1

    # If image is at end, end the game (or just reset the positions)
    if foregroundPosition>=12973:
        endGame()
    else:
        foregroundPosition+=1.5

    backgroundPosition+=.5

# Runs the game
def doRun():
    global gui
    global spawnTimer
    global score

    # Handle Monsters
    for monster in monsters:
        gui = monster.getGui().update()
        if not monster.isAlive() or monster.getGui().getRect().left<0:
            monsters.remove(monster)
        else:
            screen.blit(monster.getGui().getImage(), monster.getGui().getRect())

        if pygame.sprite.collide_rect(monster.getGui(), playerGui):
            player.loseHealth(.5)

    if len(monsters)<MaxMonsters:
        if random.random() <.20 and pygame.time.get_ticks() - spawnTimer >= 1000:
            monsters.append(Monster())
            spawnTimer = pygame.time.get_ticks()

    # Handle Player
    playerGui.update()
    if not player.isAlive():
        endGame()

    # Handle bullets
    for bullet in bullets:
        bullet.update()

        # Check bullet to monster collision
        for monster in monsters:
            if pygame.sprite.collide_rect(monster.getGui(), bullet):
                monster.loseHealth(5)
                score+=5
                if not monster.isAlive():
                    monsters.remove(monster)
                bullet.setDone()

        if bullet.isDone():
            bullets.remove(bullet)
        else:
            screen.blit(bullet.getImage(), bullet.getRect())

    # Update player on screen and score
    screen.blit(playerGui.getImage(), playerGui.getRect())
    printScore = font.render("Score:  "+str(score), True, (255, 255, 255))
    screen.blit(printScore, (450, 10))

# Main flow of program
def main():

    while run:
        moveBackgrounds()

        # Handle user input
        handleInput()

        if not inPlay:
            screen.blit(startImage, startImageRect)
        else:
            doRun()

        # Update the display
        pygame.display.update()

main()
