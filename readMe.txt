
Luke Shiffer
May 2014

//////////////////////////////
//
//	Running Program
//
/////////////////////////////

	Run from main.py
	Requires pygame to be installed on computer
		Pygame, for Windows running Python 3.3, is included in /lib
		Alternatively, download here:  https://bitbucket.org/pygame/pygame/downloads
	Developed using Python 3.3, no guarantees can be made running with an earlier version

	To start the game, press Space during the title screen
	Move left and right using A and D, respectively
	Jump using w
	Aim using the mouse cursor
	Shoot using space

	Game continues until player dies.
		No life is displayed, you must feel it out.
		Lose health by colliding with monsters.
	Game ends at a certain point.
		After a certain distance, game just ends.
		A Boss battle would be ideal OR the game never ending...  To-Do
	At end, if you have a score high enough for the top ten, the high scores are shown and you're prompted for input.
		If you do NOT reach top ten on high score, the game just goes back to title screen.
	Earn score points by shooting monsters.
		Score is shown in upper right corner. 


//////////////////////////////
//
//	How Work?
//
/////////////////////////////

	main.py
		Manages the entire program.
		Calls update on all the required objects to keep things moving.
		Runs the background and the main screen.
	Player.py
		Player Class manages the player's state.
		Instantiated and managed in main.py
	PlayerGui.py
		Player Gui, is complement to Player.
		Manages the player's visual representation.
			Tracks positions and movements.
		Instantiated and managed in main.py
	Monster.py
		Monster Class manages the monster's state.
		Instantiated and managed in main.py
		Multiple instances are allowed.
			Limited by a maximum allowed.
			Instantiated with a ~22% chance.
	MonsterGui.py
		Monster Gui, is complement to Monster.
		Manages the monster's visual representation.
			Tracks positions and movmements.
		Instantiated in Monster but used in main.py
	Bullet.py
		Bullet Class manages the bullet's state.
		Bullet also manages it's own visual representation.
		Bullets are instantiated and managed in main.py
	inputbox.py
		Retrieves text from user by displaying a popup.
		Found online.
		Changes were made to it so it would compile as the join() method for strings was being used incorrectly.
		An additional elif was added to ask() to prevent the user from entering spaces.
	scores.txt
		Text file to hold the scores.
		Only ten scores are kept track of.
		Program will NOT run without scores.txt existing in same directory as main.py
		
	Pygame was used as an alternative to tKinter in terms of the gui.
	One benefit of Pygame was the collision detection for a list of objects.
	Pygame was originally chosen for the gif, audio, and video playback capabilities but those features did not make it.
	An attempt was made to avoid the necesssity of the end-user needing to install Pygame.
		I did not want to use something that was a burden.
		Pyglet was looked at as an alternative but the poor documentation made it too cumbersome.
	All code is my own with the exception of inputbox.py sans the necessary/desired modifications. 
		

//////////////////////////////
//
//	ISSUES
//
/////////////////////////////

	High Score formatting.
		Numbers need alignment.
	Input Box
		Better styling...  Upper Cased Letters... 
	Player Movement
		Needs more refinement/smoothing out
		
	






